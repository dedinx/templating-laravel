<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use Auth;

class PertanyaanController extends Controller
{

    public function __construct(){
        $this -> middleware('auth') -> only(['create','update','edit','store']);
    }

    //=========================TUGAS PERTEMUAN 15=========================================

    public function create(){
        return view('pertanyaans.create');
    }




    public function store(Request $request){
        //dd($request->all());  

        $request -> validate([

            'judul' => 'required',
            'isi' => 'required'
        ]);
        
        /*
        $query = DB::table('pertanyaans')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        
        //=======================via methode save()=================
        $pertanyaans = new Pertanyaan;
        $pertanyaans -> judul = $request["judul"];
        $pertanyaans -> isi = $request["isi"];
        $pertanyaans -> save(); //mirip dengan insert into
        */

        //===========postingan baru menggunakan metode create via Mass Assigment========
        $pertanyaans = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "user_id" => Auth::id()
        ]);


        return redirect('/pertanyaans')-> with('success','pertanyaan berhasil disimpan');
    }

    public function index(){
      //$pertanyaans = DB::table('pertanyaans')->get();
        //dd($pertanyaans);
        $pertanyaans = Pertanyaan::all();


        return view ('pertanyaans.index', compact('pertanyaans'));
    }

    public function show($id){
        //$pertanyaan = DB::table('pertanyaans') -> where ('id',$id) -> first();
        //dd($pertanyaan);

        $pertanyaan = Pertanyaan::find($id);
        
        return view ('pertanyaans.show', compact('pertanyaan'));
    }



    public function edit($id){
        //$pertanyaan = DB::table('pertanyaans') -> where ('id',$id) -> first();

        $pertanyaan = Pertanyaan::find($id);
        return view('pertanyaans.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request){

        $request -> validate([

            'judul' => 'required',
            'isi' => 'required'

        ]);
        
        /*
        $query = DB::table('pertanyaans')
                    ->where('id',$id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);
        */
        $update = Pertanyaan::where('id',$id)->update([

            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);


        return redirect('/pertanyaans')->with('success','Berhasil update pertanyaan!');
    }


    public function destroy($id){
       // $query = DB::table('pertanyaans')->where('id',$id)->delete();
        
       Pertanyaan::destroy($id);
       
       
       return redirect('/pertanyaans') -> with ('success','Berhasil hapus pertanyaan!');
    }

}
