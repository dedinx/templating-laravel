<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('home');
});

/*

Route::get('/items', function () {
    return view('items.index');
});

Route::get('/', function () {
    return view('home');
});


Route::get('/data-tables', function () {
    return view('datatable');
});

*/

//=========================TUGAS PERTEMUAN 15=========================================

/*
//===kebutuhan create
Route::get('/pertanyaans', 'PertanyaanController@index')->name('pertanyaan.index');
Route::get('/pertanyaans/create', 'PertanyaanController@create');
Route::post('/pertanyaans','PertanyaanController@store');

//----kebutuhan show(detil)
Route::get('/pertanyaans/{id}', 'PertanyaanController@show');

//kebutuhan edit(update)
Route::get('/pertanyaans/{id}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaans/{id}', 'PertanyaanController@update');

//kebutuhan hapus(delete)
Route::delete('/pertanyaans/{id}','PertanyaanController@destroy');

*/

Route::resource('pertanyaans','pertanyaanController');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
