@extends('adminlte.master')

@section('content')
<div class="mr-3 ml-3 mt-5">
  <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Post</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaans" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">  
                    <label for="judul">Masukkan Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value=" {{ old('judul','') }}" placeholder="Enter judul" require>
                    
                    @error('judul')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi','')}}" placeholder="Enter isi" require>
                    @error('isi')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>    
                </div>
                <!-- /.box-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
  </div>
</div>
@endsection