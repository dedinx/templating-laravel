@extends('adminlte.master')

@section('content')

    <div class="mt-3 ml-3">

    <div class="card">
            <div class="card-header">
              <h3 class="card-title">Pertanyaan Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body no-padding">
            @if(session())
              <div class="alert alert-success">
                {{ session('success') }}
              </div>
            @endif
            <a class="btn btn-primary mb-2 " href="{{ route('pertanyaans.create') }}">Create</a>
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Judul</th>
                    <th>Isi</th>
                    <th style="width: 40px">Action</th>
                  </tr>

                  <tbody>
                    @forelse($pertanyaans as $key => $pertanyaan)
                      <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $pertanyaan -> judul }}</td>
                        <td>{{ $pertanyaan -> isi }}</td>
                        <td style="display:flex;">
                          <a href="{{route('pertanyaans.show',['pertanyaan'=>$pertanyaan->id])}}" class="btn btn-info btn-sm">Show</a>
                          <a href="{{route('pertanyaans.edit',['pertanyaan'=>$pertanyaan->id])}}" class="btn btn-default btm-sm ">edit</a>
                         
                          <form action="/pertanyaans/{{ $pertanyaan -> id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                         </form>
                        </td>
                      </tr>
                      @empty
                      <tr>
                      <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                      </tr>

                    @endforelse
                  </tbody>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    </div>

@endsection